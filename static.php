<?php
/*
 * $this - будующий экземпляр класса
 * self - класс где метод обьявлен
 * static - класс через который вызывается метод
 * (Вызывающий метод класс)
 */

abstract class BasicEntity{
    protected $group;

    public function __construct()
    {
        $this->group = static::getGroup();
    }

    static public function getGroup(){
        return 'default';
    }

    static public function create(){
        return new static();
    }

    static public function helloWorld(){
        return 'Hello world!';
    }

    public function sayHi(){
        return 'Hi';
    }
};



class User extends BasicEntity{

    static public function getGroup(){
        return 'users';
    }
}

class Document extends BasicEntity{

}

$user1 = User::create();
$user2 = new User();

$document = Document::create();


echo '<pre>';
print_r($user1);
print_r($user2);
print_r($document);
echo BasicEntity::helloWorld();
die();