<?php

class CSVWorker
{
    protected $file;
    protected $content;

    public function __construct($file)
    {
        if(!file_exists($file)){
            throw new Exception('File not exists' , 404);
        }
        $this->file = $file;
    }

    public function read(){
        $data = [];
        $file = fopen($this->file, 'r');
        while($row = fgetcsv($file)){
            $data[] = $row;
        }
        echo '<pre>';
        print_r($data);
        die();
    }

    static public function demo($file){
        $file = fopen($file, 'w');

        fputcsv($file, ['Title', 'Price', 'Description']);

        $data = [
            ['Iphone', 250, 'Super phone'],
            ['Macbook', 1250, 'Super laptop'],
            ['Ipad', 350, 'Super tablet'],
        ];

        foreach ($data as $row){
            fputcsv($file, $row);
        }

        fclose($file);
    }


}